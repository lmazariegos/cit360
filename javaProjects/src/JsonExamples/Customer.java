package JsonExamples;

public class Customer {
    private String first;
    private String last;
    private String email;
    private String phone;
    private String city;
    private String state;

    public String getFirst() {
        return first;
    }

    public String getLast() {
        return last;
    }

    public String getEmail() {
        return email;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getPhone() {
        return phone;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String toString(){
        return ("Name: "+ first+" "+last+
                "\nEmail: "+ email +
                "\nPhone: "+ phone +
                "\nCity/State: "+city+", "+state);
    }
}
