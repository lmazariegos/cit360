package JsonExamples;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Scanner;

public class Json {

    public static void main(String[] args){
        try {
            Customer inputCustomer = new Customer();
            ArrayList<String> userInput = getInput();

            if(userInput.get(0).isEmpty())throw new Exception("First name is empty");
            if(userInput.get(1).isEmpty())throw new Exception("Last name is empty");
            if(userInput.get(2).isEmpty())throw new Exception("Email is empty");
            if(userInput.get(3).isEmpty())throw new Exception("City is empty");
            if(userInput.get(4).isEmpty())throw new Exception("State is empty");
            if(userInput.get(5).isEmpty())throw new Exception("Phone is empty");

            inputCustomer.setFirst(userInput.get(0));
            inputCustomer.setLast(userInput.get(1));
            inputCustomer.setEmail(userInput.get(2));
            inputCustomer.setPhone(userInput.get(5));
            inputCustomer.setCity(userInput.get(3));
            inputCustomer.setState(userInput.get(4));

            String user_input = Json.stringToJson(inputCustomer);
            System.out.println(user_input);

            Customer outputCustomer = Json.jsonToString(user_input);
            System.out.println(outputCustomer);

        }catch(Exception e){
            System.out.println(e.toString());
            System.out.println("Please try one more time.");
            getInput();
        }
    }

    public static ArrayList<String> getInput(){

        ArrayList<String> userInput = new ArrayList<String>();

        Scanner answer = new Scanner(System.in);

        System.out.println("Enter a first name: ");
        String first = answer.nextLine();
        System.out.println("Enter a last name: ");
        String last = answer.nextLine();
        System.out.println("Enter an email");
        String email = answer.nextLine();
        System.out.println("Enter a city: ");
        String city = answer.nextLine();
        System.out.println("Enter a state: ");
        String state = answer.nextLine();
        System.out.println("Enter the last 4 digits of a phone number: ");
        String phone = answer.nextLine();

        userInput.add(first);
        userInput.add(last);
        userInput.add(email);
        userInput.add(city);
        userInput.add(state);
        userInput.add(phone);

        return userInput;
    }

    public static String stringToJson(Customer customer) {
        ObjectMapper mapper = new ObjectMapper();

        String input = "";
        try {
            input = mapper.writeValueAsString(customer);
            System.out.println("\nThis is my customer info to json object: ");

        }catch (JsonProcessingException e){
            System.err.println(e.toString());
        }
        return input;
    }

    public static Customer jsonToString(String input){
        String output =  input;
        ObjectMapper mapper = new ObjectMapper();
        Customer customer = null;

        try{
            customer = mapper.readValue(output, Customer.class);
            System.out.println("\nThis is my json object to string: ");

        }catch(JsonProcessingException e){
            System.err.println(e.toString());
        }
        return customer;

    }
}
