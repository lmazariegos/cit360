package javaProjects.src.JunitTesting;

import org.junit.Test;

import static org.junit.Assert.*;

public class MethodTesting{

    private final int first = 53;
    private final int second = 26;

    public static void main(String[] args){
        int first = 53;
        int second = 26;

        isNegative(first, second);
    }

    public static int isNegative(int a, int b){
        if( (a - b) < 0 ){
            System.out.println("negative number!");
        }
        else{
            System.out.println("positive number!");
        }

        return a - b;

    }

    @Test
    public void tester(){
        test1();
        test2();
        test3();
        test4();
        test5();
    }

    @Test
    public void test1(){
        int a = this.first;
        int b = this.second;
        assertTrue((b - a) < 0);
        System.out.println("assertTrue: ("+ b + " - " + a + ") < 0 = " +((b - a) < 0));

    }

    @Test
    public void test2(){
        int a = this.first;
        int b = this.second;
        assertFalse((a - b) < 0);
        System.out.println("assertFalse: (" + a + " - " + b + ") < 0 = "+((a - b) < 0));
    }

    @Test
    public void test3(){
        int a = this.first;
        int b = this.second;
        assertEquals(79, a + b);
        System.out.println("assertEquals: "+ a + " + " + b + " = " + (a + b)+" Expected: 79");
    }

    @Test
    public void test4(){
        int a = this.first;
        int b = this.second;
        assertNotNull(a);
        assertNotNull(b);
        System.out.println("assertNotNull: a = " + a);
        System.out.println("assertNotNull: b = " + b);
    }

    @Test
    public void test5(){
        int a = this.first;
        int b = this.second;
        assertNotEquals(25, a-b);
        System.out.println("assertNotEquals: " + a + " - " + b + " = " + (a-b)+" Expected: 25");
    }

}