package javaProjects.src.Exceptions;

import java.io.*;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String args[]) throws Exception {
        try {
            inputNumbers();
        } 
        catch (Exception e ) {
            System.out.println("Please enter a valid number.");
            try{
                inputNumbers();
            }
            catch(Exception x ){
                System.out.println(x);
                inputNumbers();
            }
        }
    }

    public static void inputNumbers() throws Exception {
        Scanner numbers = new Scanner(System.in);
        System.out.println("We will divide two numbers. Enter your first number:");
        int integer = numbers.nextInt();
        System.out.println("Thank you. Enter your second number:");

        int divisor = numbers.nextInt();
        if(divisor <= 0) {
            throw new Exception("You cannot divide by zero! Try again");
        }
        System.out.println("Success! \n" + integer + "/" + divisor + " = " + integer/divisor);

        numbers.close();
    }
}
