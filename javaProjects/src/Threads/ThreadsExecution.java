package Threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadsExecution{

    public static void main(String[] args){

        ExecutorService myService = Executors.newFixedThreadPool(3);

        Threads thread1 = new Threads("Who's on first", 45, 100);
        Threads thread2 = new Threads("Go ahead and tell me.", 40, 90);
        Threads thread3 = new Threads("Who is on first!", 35, 80);
        Threads thread4 = new Threads("You ain't tellin' me nothin'. I'm askin' you, who's on first?!", 30, 70);
        Threads thread5 = new Threads("What's the guy's name on first?", 25, 60);
        Threads thread6 = new Threads("What's on second", 20, 50);
        Threads thread7 = new Threads("I'm not askin' you who's on second!", 15, 40);
        Threads thread8 = new Threads("I don't know", 10, 30);
        Threads thread9 = new Threads("I don't know's on third", 5, 20);

        myService.execute(thread1);
        myService.execute(thread2);
        myService.execute(thread3);
        myService.execute(thread4);
        myService.execute(thread5);
        myService.execute(thread6);
        myService.execute(thread7);
        myService.execute(thread8);
        myService.execute(thread9);

        myService.shutdown();
    }
}