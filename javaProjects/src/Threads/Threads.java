package Threads;

import java.util.Random;

public class Threads implements Runnable{
    private String name;
    private int number;
    private int live;
    private int rand;

    public Threads(String name, int number, int live){
        this.name = name;
        this.number = number;
        this.live = live;

        Random random = new Random();
        this.rand = random.nextInt(500);
    }
    public void run(){
        System.out.println("\n\nExecuting with these Parameters: Name = "+ name + " Number = "+ number+" Live = "
                +live+" Rand Num "+rand+"\n\n");
        for(int count = 1; count < rand; count ++){
            if(count % number == 0){
                System.out.println(name+"\n");
                try{
                    Thread.sleep(live);
                }catch (InterruptedException e){
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println("\n\n"+name+ " is done.\n\n");
    }
}