package JavaCollections;

import java.util.*;

public class Collections {

    public static void main(String[] args){

        System.out.println("\nThis Queue will order an unordered list of items. This is my Queue:\n");
        Queue animals = new PriorityQueue();

        animals.add("Bear");
        animals.add("Flamingo");
        animals.add("Cat");
        animals.add("Elephant");
        animals.add("Dog");
        animals.add("Aardvark");


        Iterator iterator = animals.iterator();
        while (iterator.hasNext()){
            System.out.println(animals.poll());
        }

        System.out.println("\nThis List will output the objects as they lay. This is my List:\n");
        List poem = new ArrayList();
        poem.add("Roses");
        poem.add("are");
        poem.add("Red");
        poem.add("Violets");
        poem.add("are");
        poem.add("Blue");
        for(Object str : poem){
            System.out.println((String)str);
        }

        System.out.println("\nThis hash set hashes the objects, there is no way of ordering them. This is my Set:\n");
        HashSet<Integer> numbers = new HashSet<Integer>();
        numbers.add(100);
        numbers.add(300);
        numbers.add(1000);
        numbers.add(50);
        numbers.add(10);
        numbers.add(527);

        Iterator iterator2 = numbers.iterator();
        while (iterator2.hasNext()){
            System.out.println(iterator2.next());
        }

        System.out.println("\nThis Tree set will eliminate any duplicate objects. This is my Tree:\n");
        TreeSet<String> stutter = new TreeSet<String>();
        stutter.add("Did");
        stutter.add("Did");
        stutter.add("Did");
        stutter.add("Did");
        stutter.add("I");
        stutter.add("Stutter?");

        Iterator iterator3 = stutter.iterator();
        while (iterator3.hasNext()){
            System.out.println(iterator3.next());
        }

    }

}
