<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Java Servlets</title>
</head>
<body>
<h1><%= "Wanna search some shows?" %>
</h1>
<br/>
<a href="${pageContext.request.contextPath}/servletExamples.html">Click here to start!</a>
</body>
</html>