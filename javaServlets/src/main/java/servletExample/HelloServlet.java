package servletExample;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.PriorityQueue;

//import JsonBodyHandler.JsonBodyHandler;

@WebServlet(name = "HelloServlet", urlPatterns = {"/HelloServlet"})
public class HelloServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            PrintWriter output = response.getWriter();

            String firstShow = request.getParameter("firstShow");
            String secondShow = request.getParameter("secondShow");
            String thirdShow = request.getParameter("thirdShow");
            String fourthShow = request.getParameter("fourthShow");
            String fifthShow = request.getParameter("fifthShow");

            if (firstShow.isEmpty() == true && secondShow.isEmpty() == true  &&
                    thirdShow.isEmpty() == true  && fourthShow.isEmpty() == true  &&
                    fifthShow.isEmpty() == true ){
                response.setContentType("text/html");
                output.println("<html><head></head><body>");
                output.println("<h1>Please Fill in a show title for information.</h1>");
                output.println("</body></html>");
                Thread.sleep(5000);
                throw new IOException("Please Fill in a show title for information.");
            }
            PriorityQueue<String> shows = new PriorityQueue();

            if(firstShow.isEmpty() == false) {
                shows.add(firstShow);
            }
            if(secondShow.isEmpty() == false ){
                shows.add(secondShow);
            }
            if(thirdShow.isEmpty() == false ){
                shows.add(thirdShow);
            }
            if(fourthShow.isEmpty() == false) {
                shows.add(fourthShow);
            }
            if(fifthShow.isEmpty() == false){
                shows.add(fifthShow);
            }

            response.setContentType("text/html");
            output.println("<html><head></head><body>");
            output.println("<h1>Here is Your List of Shows and Their Information</h1>");
            Iterator iterator = shows.iterator();
            int i = 1;
            while (iterator.hasNext()) {
                if(shows.isEmpty() == true) {
                    continue;
                }
                Object value = iterator.next();
                output.println("<p>Show " + i + ": " + value + "</p>");
                output.println("This is supposed to output show information for the user.");
// URL Access help from https://github.com/mjg123/java-http-clients/tree/master/src/main/java/com/twilio
// encoding help from https://www.baeldung.com/java-url-encoding-decoding

                URL url = new URL("https://api.tvmaze.com/search/shows?q="+URLEncoder.encode(value.toString(), StandardCharsets.UTF_8.toString())+"&embed=episodes");
                output.println("\n"+"<a href="+url+">Click for JSON Return</a>");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                System.out.println(connection);
                connection.setRequestProperty("accept", "application/json");
                InputStream responseStream = connection.getInputStream();
                System.out.println(responseStream);
//                  this is not working correctly
//                ObjectMapper mapper = new ObjectMapper();
//                APOD apod = mapper.readValue(responseStream, APOD.class);
//                System.out.println(apod.title);
//                System.out.println(apod.url);
                i++;
            }

            output.println("<h1>Thank you for your request, Enjoy Your Shows!</h1>");
            output.println("</body></html>");
        }
        catch(IOException | InterruptedException x ){
            System.out.println(x);
            response.setContentType("text/html");
            String redirect = new String("/javaServlets_war_exploded/servletExamples.html");
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", redirect);
        }
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
//         Hello
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>Sorry! I am not working :(</h1>");
        out.println("</body></html>");
    }
}